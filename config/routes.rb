Rails.application.routes.draw do

  namespace 'api' do
    namespace 'v1' do
      resources :survivors
      put 'survivors/:id/update_location', to: 'survivors#update'
      put 'survivors/:id/trade/', to: 'survivors#trade'

      post 'resources', to: 'resources#create'
      put 'resources/:id', to: 'resources#update'
      patch 'resources/:id', to: 'resources#update'

      get 'dashboard', to: 'dashboad#index'
    end
  end
end
