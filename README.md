# README

## This is Zombie Api

We have some endpoints with namespace 'api/v1'

They are:
```
GET    /api/v1/survivors                     api/v1/survivors#index
POST   /api/v1/survivors                     api/v1/survivors#create
GET    /api/v1/survivors/:id                 api/v1/survivors#show
PATCH  /api/v1/survivors/:id                 api/v1/survivors#update
PUT    /api/v1/survivors/:id                 api/v1/survivors#update
DELETE /api/v1/survivors/:id                 api/v1/survivors#destroy
PUT    /api/v1/survivors/:id/update_location api/v1/survivors#update
PUT    /api/v1/survivors/:id/trade           api/v1/survivors#trade
POST   /api/v1/resources                     api/v1/resources#create
PUT    /api/v1/resources/:id                 api/v1/resources#update
PATCH  /api/v1/resources/:id                 api/v1/resources#update
GET    /api/v1/dashboard                     api/v1/dashboad#index
```
You can check this use in /doc/api and use some json examples:

### Trade items:
`PUT http://localhost:3000/api/v1/survivors/:id/trade`
```
{
  "items": [
    {
      "item": "Water",
      "quantity": 2
    },
    {
      "item": "Food",
      "quantity": 1
    }
  ]
}
```
### Create a survivor
`POST http://localhost:3000/api/v1/survivors`
```
{
  "name": "Fulano",
  "age": 26,
  "gender": "Homem trans",
  "latitude": 26.1793003082275,
  "longitude": -80.149299621582,
  "infected": false,
  "resources_attributes": [
    {
      "item": "Water"
    },
    {
      "item": "Water"
    },
    {
      "item": "Food"
    }
  ]
}
```
### Update locale
`PUT http://localhost:3000/api/v1/survivors/:id/update_location`
```
{
  "latitude": 16.6890456,
  "longitude": -49.2588003
}
```

### Report a survivor
`PUT http://localhost:3000/api/v1/survivors/:id`
```
{
  "reports": ["infected", "infected", "infected", "infected"]
}
```

### Check the dashboard
`GET http://localhost:3000/api/v1/dashboard`

### Check all survivors
`GET http://localhost:3000/api/v1/survivors`

### Check one survivor
`GET http://localhost:3000/api/v1/survivors/:id`