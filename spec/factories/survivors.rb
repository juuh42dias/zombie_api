FactoryBot.define do
  factory :survivor do
    name { FFaker::Name.name }
    age 18
    gender do
      ['Não Binário', 'Homem cis', 'Homem trans',
       'Mulher cis', 'Mulher trans'].sample
    end
    latitude { FFaker::Geolocation.lat }
    longitude { FFaker::Geolocation.lng }
    infected false
    reports []

    trait :with_resources do
      after(:create) do |survivor|
        create_list :resource, 4, survivor_id: survivor.id
      end
    end

    trait :infected do
      infected true
    end
  end
end
