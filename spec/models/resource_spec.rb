require 'rails_helper'

RSpec.describe Resource, type: :model do

  let(:survivor) { FactoryBot.create(:survivor) }
  let(:resource) { FactoryBot.create(:resource, survivor_id: survivor.id) }

  subject { resource }

  it { expect(subject).to belong_to :survivor }
  it { expect(subject).to validate_presence_of :item }

  describe '#check_infected_survivor' do
    let(:infected_survivor) { FactoryBot.create(:survivor, :infected) }
    let(:infected_resource) { FactoryBot.build(:resource, survivor_id: infected_survivor.id)}
    
    it 'returns error when infected' do   
      expect{infected_resource.save!}.to raise_error
    end

    let(:resource) { FactoryBot.build(:resource, survivor_id: survivor.id) }
    it 'returns true when not infected' do
      expect(resource.save!).to be_truthy
    end
  end
end
