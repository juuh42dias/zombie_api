require 'rails_helper'
require "rspec_api_documentation/dsl"


resource "Resources" do
  header "Accept", "application/json"
  header "Content-Type", "application/json"

  let(:survivor) { FactoryBot.create(:survivor) }
  let(:resource) { FactoryBot.create(:resource, survivor_id: survivor.id) }

  # POST create
  post 'api/v1/resources' do
    survivor = FactoryBot.create(:survivor)
    resource = FactoryBot.build(:resource, survivor_id: survivor.id)

    parameter :resource, resource
    let(:resource) { resource }

    parameter :item, resource.item
    let(:item) { resource.item }

    parameter :survivor_id, resource.survivor_id
    let(:survivor_id) { resource.survivor_id }

    let(:raw_post) { params.to_json }

    example_request 'Create a resource' do
      do_request

      expect(status).to eq(201)
    end
  end

  # PUT update
  put 'api/v1/resources/:id' do
    before do
      
    end

    let(:id) { resource.id }

    let(:new_survivor) { FactoryBot.create(:survivor) }

    item = ['Water', 'Food', 'Medication', 'Ammunition'].sample

    parameter :item, item
    let(:item) { item }

    parameter :survivor_id, "1"
    let(:survivor_id) { new_survivor.id }

    let(:raw_post) { params.to_json }

    example_request 'Update item and survivor id resources' do
      do_request

      expect(status).to eq(200)
    end
  end
end
