module TraderService
  extend ActiveSupport::Concern

  TABLE_ITEM_POINTS = {
      "Water" => 4,
      "Food" => 3,
      "Medication" => 2,
      "Ammunition" => 1
    }.freeze

  def check_items_on_your_resources(user_id = nil, resources = [])
    survivor_resources = Survivor.find(user_id).resources

    resources.map do |resource|
      resource_item_count = survivor_resources.where(item: resource[:item].capitalize).count
      resource[:quantity] <= resource_item_count
    end
  end

  def trade_resource_by_user(sender = nil, receiver_resources = [], sender_resources = [])
    sender = Survivor.find(sender.id)
    receiver = Survivor.find(receiver_resources[0][:survivor_id])

    unless sender.id == receiver.id
      sender_resources.each do |resource|
        resource[:quantity].times do
          sender_resource = sender.resources.find_by(item: resource[:item])
          sender_resource.update!(survivor_id: receiver.id)
          sender_resource.reload
        end
      end

      receiver_resources.each do |resource|
        resource.update!(survivor_id: sender.id)
        resource.reload
      end
    end
  end

  def get_only_equivalent_resources(resources = [])
    survivor_trade = get_survivors_with_possible_trade_resources(resources).sample
    max_point = get_points_for_trade_by_resources(resources)
    resources_for_trade = []
    trade_points = 0    
    final_result = nil

    survivor_trade.resources.each do |resource|
      trade_points += TABLE_ITEM_POINTS.fetch(resource.item)
      resources_for_trade.push resource if trade_points <= max_point
      final_result = resources_for_trade if trade_points == max_point
    end
    final_result
  end

  private

  def get_valuation_for_trade(trader)
    items = trader.resources.pluck(:item)
    quantity = items.count

    trader_points = 0
    items.each do |item|
      point = TABLE_ITEM_POINTS.fetch(item)
      trader_points += point
    end
    trader_points
  end

  def get_points_for_trade_by_resources(resources = [])
    trader_points = 0
    resources.each do |resource|
      item = resource[:item]
      quantity = resource[:quantity]

      trader_points += TABLE_ITEM_POINTS.fetch(item) * quantity
    end
    trader_points
  end

  def get_survivors_with_possible_trade_resources(resources = [])
    survivors = []
    Survivor.all.each do |survivor|
      next if survivor.infected
      if get_valuation_for_trade(survivor) >= get_points_for_trade_by_resources(resources)
        survivors.push survivor
      end
    end
    survivors
  end
end
