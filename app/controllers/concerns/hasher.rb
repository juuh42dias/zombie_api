module Hasher
  def survivor_hash(param:)
    {
      survivor: {
        name: param.name,
        age: param.age,
        gender: param.gender,
        latitude: param.latitude,
        longitude: param.longitude,
        infected: param.infected,
        # reported: params.reports.flatten.count,
        resources: param.resources
      }
    }
  end
end