module Api::V1
  class DashboadController < ApplicationController
    def index
      render json: Dashboard.show
    end
  end
end