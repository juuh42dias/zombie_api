module Api::V1
  class SurvivorsController < ApplicationController
    include Hasher
    include TraderService

    before_action :set_survivor, only: %i[show update destroy, push_reports, trade]
    before_action :push_reports, only: :update

    # GET /survivors
    def index
      @survivors = Survivor.order(id: :asc)
      survivors_with_resources = []
      @survivors.each do |survivor|
        survivors_with_resources.push (survivor_hash(param: survivor))
      end

      render json: survivors_with_resources.to_json
    end

    # GET /survivors/1
    def show
      @survivor = survivor_hash(param: @survivor)
      render json: @survivor
    end

    # POST /survivors
    def create
      @survivor = Survivor.new(survivor_params)

      if @survivor.save
        render json: @survivor, status: :created
      else
        render json: { errors: @survivor.errors }, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /survivors/1
    def update
      if @survivor.update(update_params)
        render json: @survivor
      else
        render json: { errors: @survivor.errors }, status: :unprocessable_entity
      end
    end

    def trade
      resources = params.fetch(:items)
      resources_for_trade = get_only_equivalent_resources(resources)

      if check_items_on_your_resources(@survivor.id, resources).include? false
        render json: { message: "Item(s) not found in your inventory" }, status: :unprocessable_entity
      elsif resources_for_trade.blank?
        render json: { message: "There are no survivors with equivalent punctuation for the moment" }, status: :unprocessable_entity
      else
        trade_resource_by_user(@survivor, resources_for_trade, resources)
        render json: { message: "Exchanged with success!" }, status: :ok
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_survivor
      @survivor = Survivor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def survivor_params
      params.permit(:name, :age, :gender, :latitude, :longitude, :infected,
                                       resources_attributes: %i[id item point _destroy])
    end

    def update_params
      params.permit(:latitude, :longitude, :reports)
    end

    def push_reports
      if params[:reports].present?
        reports = params[:reports].compact
      end
      @survivor.reports.push reports unless reports.blank?
    end
  end
end