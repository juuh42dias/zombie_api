class Survivor < ApplicationRecord
  has_many :resources, dependent: :destroy
  accepts_nested_attributes_for :resources,
                                reject_if: :blank_or_old_record

  validates_associated :resources
  validates :age, numericality: { equal_or_greater_than: 0 }
  validates :name, :age, presence: true

  before_update :set_infected_when_reported

  def set_infected_when_reported
    self.infected = true if reports.flatten.count >= 3
  end

  private

  def blank_or_old_record(params)
    survivor_id = params[:survivor_id].presence
    (survivor_id.present? && new_record?) || (survivor_id.blank? && !new_record?)
  end
end
