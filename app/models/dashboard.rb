class Dashboard
  TABLE_ITEM_POINTS = {
    "water" => 4,
    "food" => 3,
    "medication" => 2,
    "ammunition" => 1
  }.freeze

  def self.show
    {
      "Infected average" => infected_average,
      "Non infected average" => non_infected_average,
      "Resources by survivors" => resources_by_survivor_average,
      "Lost points" => lost_points
    }
  end

  private

  class << self
    def infected_average
      "#{Survivor.where(infected: true).count * 100.0 / Survivor.count}%"
    end

    def non_infected_average
      "#{Survivor.where(infected: false).count * 100.0 / Survivor.count}%"
    end

    def resources_by_survivor_average
      items = ['Water', 'Food', 'Medication', 'Ammunition']
      items_by_survivor = {}
      items.each do |item|
        quantity = Resource.where(item: item).count / Survivor.count
        items_by_survivor.merge!({item=> quantity})
      end
      items_by_survivor
    end

    def lost_points
      infected_survivors = Survivor.where(infected: true)
      
      lost_points = 0
      infected_survivors.each do |survivor|
        survivor.resources.each do |resource|
          lost_points += TABLE_ITEM_POINTS[resource.item.downcase]
        end
      end
      lost_points
    end
  end
end
